<?php

$logfile = 'tivo_log.txt';
$dater = date('m-d-Y h:m:s');
$key = 'jhl243hg52h3g4hkjkjh';

include_once('functions.php');

$channels = array('Hallmark' => 854,
				  'ESPN' => 832,
				  'ABC' => 803,
				  'CBS' => 807,
				  'NBC' => 809,
				  'FOX' => 808
				  );

$tivo1 = '192.168.1.109';
$action = 'SETCH';

$code = $_GET['code'];
$command = $_GET['command'];

if(array_key_exists($command, $channels)){
	//channel is in array, change channel and log
	if ($code == '' || $command == '' || $code != $key){
		
		//something went wrong with code or command, log it
		$msg = $dater . ' - Code or command error.  Code : ' . $code . ' - Command : ' . $command;
		$msg .= "\n";
		file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
	
	}else{
		$chan = $channels[$command];

		tivoBox_SEND_CMD($tivo1,$action,$chan,false);
		$msg = $dater . ' - Channel changed to ' . $command . ' - ' . $chan;
		$msg .= "\n";
		file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
	}

}else{
	//channel not known, log it
	$msg = $dater . ' - Channel : |' . $command . '| unknown';
	$msg .= "\n";
	file_put_contents($logfile, $msg, FILE_APPEND | LOCK_EX);
}
