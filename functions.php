<?php

function tivoBox_SEND_CMD($address,$command='PO',$parameter=false,$param_first=true) {
    $fp = fsockopen($address, 31339, $errno, $errstr, 30);
    if (!$fp) {
        echo __FUNCTION__."() ERROR: $errstr ($errno), planned command was \"$command\"!\n";
        return false;
    } else {
        $cmd = '';
        if (! $parameter) {
            $cmd = $command;
        } else {
            if ($param_first) {
                $cmd = $parameter.$command;
            } else {
                $cmd = $command . ' ' . $parameter;
            }
        }
        $cmd .= "\r\n";


        fwrite($fp, $cmd);
        echo $cmd;


        //$out = fgets($fp);
        //fclose($fp);
        // Cool-down time
        sleep(2);
        return true;
    }
    return false;
}
// Send a command to the TiVo unit requesting the viewed channel decrement
function tivoBox_setChnDec($address) {
    tivoBox_SEND_CMD($address,'IRCODE CHANNELDOWN');
}
// Send a command to the TiVo unit requesting the viewed channel increment
function tivoBox_setChnInc($address) {
    tivoBox_SEND_CMD($address,'IRCODE CHANNELUP');
}