# GoogleHome_Tivo

You can use this to change channels on your Tivo via Google Home.  
You will need to integrate your Google Home with IFTTT and specify the verbiage you want to use via IFTTT.  
Then specify a web request as the THAT part of the equation and point it to the tivo.php file.  Include the GET variables CODE and COMMAND, where CODE is a unique identifier (random code specified at the top of tivo.php) and command is a channel name.
You will need to change the channel array to fit your local programming assignments.
Change the IP specified for the Tivo box in tivo.php to reflect the IP address of the Tivo on your local network.  Also, make sure you have network control enabled in the Tivo settings.
